import mechanize
from bs4 import BeautifulSoup #as soup
import getpass
import argparse
from time import sleep
import wget
import os


parser = argparse.ArgumentParser(description="Bulk download works from AO3")
parser.add_argument("-u", "--user", help="username")
parser.add_argument("-p", "--password", help="password")
# parser.add_argument("-b", "--bookmarks", help="download your bookmarks", action="store_true")
# parser.add_argument("-L", "--marked-for-later", help="download works marked for later", action="store_true")
# parser.add_argument("-U", "--url", help="download works from a custom URL")
parser.add_argument("-l", "--list", help="list works without downloading", action="store_true")
parser.add_argument("-a", "--azw3", help="download as azw3", action="store_true")
parser.add_argument("-m", "--mobi", help="download as mobi", action="store_true")
parser.add_argument("-e", "--epub", help="download as epub", action="store_true")
parser.add_argument("-P", "--pdf", help="download as pdf", action="store_true")
parser.add_argument("-H", "--html", help="download as html", action="store_true")
args = parser.parse_args()

if not args.user: un = input("AO3 username: ").strip()
else: un = args.user
if not args.password: pw = getpass.getpass("AO3 password: ")
else: pw = args.password

br = mechanize.Browser()
br.set_handle_robots(False)

# log in
br.open("https://archiveofourown.org/users/login")
br.select_form(action="/users/login")
br['user[login]'] = un
br['user[password]'] = pw
if br.submit().getcode() == 200:
    login_ok = True
    print("Login successful!")
else:
    login_ok = False
    print("Login failed")



def download_work(url, title, author):#, azw3=False, mobi=False, epub=False, html=False, pdf=False):
    response = br.open(url)
    page_content = response.read()
    work_url = BeautifulSoup(page_content, features='lxml').find("li", class_="download")
    print("  Downloading work... ", end='')
    if args.azw3:
        ext = ".azw3"
        if title + ext not in os.listdir("."):
            dl_url = "https://archiveofourown.org" + work_url.find_all("a")[1]["href"]
            filename = wget.download(dl_url)
            # br.retrieve(dl_url, 'fics/'+title+' by '+author+'.epub')[0]
            print("filename: " + filename + ", done!", end='')
        else:
            print("already downloaded!")
    if args.epub:
        ext = ".epub"
        if title + ext not in os.listdir("."):
            dl_url = "https://archiveofourown.org" + work_url.find_all("a")[2]["href"]
            # print(dl_url)
            filename = wget.download(dl_url)
            # br.retrieve(dl_url, 'fics/'+title+' by '+author+'.epub')[0]
        else:
            print("already downloaded!")
        print("filename: " + filename + ", done!", end='')
    if args.mobi:
        ext = ".mobi"
        if title + ext not in os.listdir("."):
            dl_url = "https://archiveofourown.org" + work_url.find_all("a")[3]["href"]
            filename = wget.download(dl_url)
            # br.retrieve(dl_url, 'fics/'+title+' by '+author+'.epub')[0]
            print("filename: " + filename + ", done!", end='')
        else:
            print("already downloaded!")
    if args.pdf:
        ext = ".pdf"
        if title + ext not in os.listdir("."):
            dl_url = "https://archiveofourown.org" + work_url.find_all("a")[4]["href"]
            filename = wget.download(dl_url)
            # br.retrieve(dl_url, 'fics/'+title+' by '+author+'.epub')[0]
            print("filename: " + filename + ", done!", end='')
        else:
            print("already downloaded!")
    if args.html:
        ext = ".html"
        if title + ext not in os.listdir("."):
            dl_url = "https://archiveofourown.org" + work_url.find_all("a")[5]["href"]
            filename = wget.download(dl_url)
            # br.retrieve(dl_url, 'fics/'+title+' by '+author+'.epub')[0]
            print("filename: " + filename + ", done!", end='')
        else:
            print("already downloaded!")
    print("")


def get_works_on_page(page_content):
    works = BeautifulSoup(page_content, features='lxml').find_all("li", class_="blurb")
    for w in works:
        author = w.find('a', rel="author")
        if author == None:
            print("• [deleted work]\n")
            continue
        else: author = author.contents[0]
        title = w.find('h4', class_="heading").find('a')
        print("• " + str(title.text) + " by " + str(author))
        work_url = "https://archiveofourown.org" + title['href']
        print("  " + work_url + "\n")
        if not args.list: download_work(work_url, title.text, str(author))
        sleep(5)



def get_all_works(url=''):
    response = br.open(url)
    page_content = response.read()
    get_works_on_page(page_content)
    nxt = BeautifulSoup(page_content, features='lxml').find("a", rel="next")
    u = url
    while nxt != None:
        nxt = BeautifulSoup(page_content, features='lxml').find("a", rel="next")
        u = "https://archiveofourown.org" + nxt['href']
        get_works_on_page(br.open(u).read())
        print("PAGE: " + u)


# remove the '#' to un-comment a line
# get_all_works("https://archiveofourown.org/users/YOUR_USERNAME/readings?show=to-read")
# get_all_works('https://archiveofourown.org/users/YOUR_USERNAME/bookmarks?page=20')
