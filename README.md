# AO3 bulk downloader

This is a simple utility to download multiple works easily from AO3 (e.g. your bookmarks, works you've marked for later, etc).

<!-- TOC -->

- [Warning -- READ THIS FIRST!](#warning----read-this-first)
- [How does it work?](#how-does-it-work)
- [Getting started](#getting-started)
- [Usage](#usage)
    - [Example usage](#example-usage)
- [Feedback & issues](#feedback--issues)
    - [Known issues](#known-issues)
- [License](#license)

<!-- /TOC -->

## Warning -- READ THIS FIRST!
This program **DOES NOT COMPLY** with AO3's Terms of Service. ***Your account or IP may be banned*** for using it. **USE IT AT YOUR OWN RISK.**

This program is an unofficial tool that is in no way related to or affiliated with AO3. By using it, you agree not to hold the creator responsible or accountable for any consequences, negative or otherwise, that may arise from its use.

This program is distributed in the hope that it will be useful, but **WITHOUT ANY WARRANTY**; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [License](#license) section and the GNU General Public License for more details.



## How does it work?
Normally you would log into your AO3 account, go to (say) your bookmarks page, click every work there, and then download the work in your preferred format, right? This program basically does all that clicking for you. It's a bot.

According to <a href="https://archiveofourown.org/tos_faq">AO3's terms of service</a>,

>Do you have a policy on bots or scraping? These are ways of extracting information from or indexing websites.
>
>Using bots or scraping is not against our Terms of Service unless it relates to our guidelines against spam or other activities. However, we do reserve the right to implement robots.txt or other protocols limiting what bots can do, or to notify you and ask you to discontinue if a bot or scraping program is causing problems for the site.

TL;DR: basically they want us to respect AO3's <a href="https://archiveofourown.org/robots.txt">robots.txt</a>, which bans bots from accessing the work and download pages.

This program ignores the `robots.txt` and accesses those pages anyway. It's fine in limited doses in my experience -- say, downloading 20 works at a time/per day; I haven't tried anything beyond that. ***USE AT YOUR OWN RISK!***

## Getting started

1. Install Python 3 and pip on your computer.
2. Download this repository as a zip or clone it with git.
3. Navigate to the folder and install the dependencies:
```
pip3 install -r requirements.txt
```
or
```
py -m pip install -r requirements.txt
```
4. Run the program from the shell as:
```
python3 ao3-bulk-dl.py [your options]
```
or
```
py ao3-bulk-dl.py [your options]
```
Check the [Usage](#usage) section for options you can use.


## Usage

Open the file `ao3-bulk-dl.py` with your favourite text editor and edit the last line. For example, to download your marked-for-later works, you need the line:
```
get_all_works("https://archiveofourown.org/users/YOUR_USERNAME/readings?show=to-read")
```

For your bookmarks, that would be:
```
get_all_works('https://archiveofourown.org/users/YOUR_USERNAME/bookmarks?page=20')
```


The following options are available. You might have to substitute `python` in the command with `py` or `python3`, as is appropriate for your OS and Python installation.
```
$ python ao3-bulk-dl.py -h
usage: ao3-bulk-dl.py [-h] [-u USER] [-p PASSWORD] [-l] [-a] [-m] [-e] [-P] [-H]

Bulk download works from AO3

optional arguments:
  -h, --help            show this help message and exit
  -u USER, --user USER  username
  -p PASSWORD, --password PASSWORD
                        password
  -l, --list            list works without downloading
  -a, --azw3            download as azw3
  -m, --mobi            download as mobi
  -e, --epub            download as epub
  -P, --pdf             download as pdf
  -H, --html            download as html
```

### Example usage
1. List works. (This will ask you to login interactively)
```
python ao3-bulk-dl.py -l
```
2. Download works as epub for an account with USERNAME and PASSWORD
```
python ao3-bulk-dl.py -e -u USERNAME -p PASSWORD
```


## Feedback & issues

Due to limited free time, I may not update this program in the future. So, if you want any changes/improvements/new features, I encourage you fork the project and implement them yourself.

Even so, feel free to file an issue if you run into any bugs. Feedback and suggestions for improvement are welcome. I can't promise I'll get to them in a timely manner, though.


### Known issues

- does not download bookmarked series
- if there are multiple works with the same name, only the first one encountered will be downloaded


## License
AO3 bulk downloader, Copyright (C) 2019  antarcticite

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
